import asyncio

from workflows import workflowA as wf

if __name__ == "__main__":
    # _, _, c = asyncio.run(wf.grab_data(write=True))
    # asyncio.run(wf.pull_images(c))
    asyncio.run(
        wf.pull_bold_data_async(
            taxa=["Viviennea", "Phaegoptera"],
            geo=["Costa Rica|Nicaragua", "Brazil|Peru"],
        )
    )
