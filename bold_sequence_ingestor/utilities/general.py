import functools
from typing import Any

from aiohttp import ClientSession, ClientTimeout

from models.base import BOLDApiCall, BOLDVersion
from models.bold3 import BOLDv3SearchEnum, BOLDv3SingleSearch
from models.bold4 import (
    BOLDv4Result,
    BOLDv4SearchEnum,
    BOLDv4SingleSearch,
    BOLDv4CollectingEvent,
    BOLDv4Coordinate,
    BOLDv4Copyright,
    BOLDv4MediaItem,
    BOLDv4Result,
    BOLDv4SpecimenDesc,
    BOLDv4SpecimenIdentifiers,
    BOLDv4Taxon,
    BOLDv4Taxonomy,
)


async def compose_elements(elements: str | list[str]) -> str:
    return "|".join(elements).lower() if elements else ""


async def compose_search(
    version: BOLDVersion = BOLDVersion("v4"),
    search_type: str = "specimen",
    **kwargs: str,
) -> BOLDv3SingleSearch | BOLDv4SingleSearch:
    if kwargs is not None:
        if version.value == "v3":
            return BOLDv3SingleSearch(
                search_type=BOLDv3SearchEnum(search_type).value,
                taxon=await compose_elements(kwargs.get("taxon")),
                ids=await compose_elements(kwargs.get("ids")),
                bins=await compose_elements(kwargs.get("bins")),
                container=await compose_elements(kwargs.get("container")),
                institutions=await compose_elements(kwargs.get("institutions")),
                researchers=await compose_elements(kwargs.get("researchers")),
                geo=await compose_elements(kwargs.get("geo")),
                marker=await compose_elements(kwargs.get("marker")),
                fileFormat=kwargs.get(  # just set a default instead
                    "fileFormat"
                ).lower()
                if kwargs.get("fileFormat")
                else "tsv",
            )
        else:
            return BOLDv4SingleSearch(
                search_type=BOLDv4SearchEnum(search_type).value,
                taxon=await compose_elements(kwargs.get("taxon")),
                ids=await compose_elements(kwargs.get("ids")),
                bins=await compose_elements(kwargs.get("bins")),
                container=await compose_elements(kwargs.get("container")),
                institutions=await compose_elements(kwargs.get("institutions")),
                researchers=await compose_elements(kwargs.get("researchers")),
                geo=await compose_elements(kwargs.get("geo")),
                marker=await compose_elements(kwargs.get("marker")),
                dataType=(  # just set a default instead
                    kwargs.get("dataType").lower()
                    if kwargs.get("dataType")
                    else "overview"
                ),
                specimen_download=(  # just set a default instead
                    kwargs.get("specimen_download").lower()
                    if kwargs.get("specimen_download")
                    else "json"
                ),
                fileFormat=(  # just set a default instead
                    kwargs.get("fileFormat").lower()
                    if kwargs.get("fileFormat")
                    else "json"
                ),
            )


async def compose_api_call(
    search: BOLDv3SingleSearch | BOLDv4SingleSearch,
) -> BOLDApiCall:
    api_version = search.version.lower()
    api_address = ".boldsystems.org/index.php/API_Public/"
    api_search_type = f"{search.search_type}?"
    base_url = f"http://{api_version}{api_address}{api_search_type}"
    api_call = (
        (
            f"{base_url}"
            f"taxon={search.taxon}&"
            f"ids={search.ids}&"
            f"bin={search.bins}&"
            f"container={search.container}&"
            f"institutions={search.institutions}&"
            f"researchers={search.researchers}&"
            f"geo={search.geo}&"
            f"marker={search.marker}&"
            f"format={search.fileFormat}"
        )
        .strip()
        .replace(" ", "%20")
    )
    return BOLDApiCall(
        url=api_call,
    )


async def get_bold_data(search: BOLDv3SingleSearch | BOLDv4SingleSearch):
    api_call = await compose_api_call(search)
    timeout_secs = 1500
    session_timeout = ClientTimeout(total=None, sock_connect=5, sock_read=timeout_secs)
    try:
        async with ClientSession(timeout=session_timeout) as session:
            async with session.get(
                api_call, allow_redirects=False, timeout=timeout_secs
            ) as resp:
                if resp.ok:
                    content = await resp.text()
                else:
                    content = None
    except TimeoutError as e:
        print(e)
        content = None
    return (search.fileFormat, api_call, content)


async def compose_parent_attribute_chain(key: str) -> str:
    if key in BOLDv4Result.__fields__.keys():
        return ""
    elif key in BOLDv4SpecimenIdentifiers.__fields__.keys():
        return "specimen_identifiers."
    elif key in BOLDv4SpecimenDesc.__fields__.keys():
        return "specimen_desc."
    elif key in BOLDv4CollectingEvent.__fields__.keys():
        return "collection_event."
    elif key in BOLDv4Coordinate.__fields__.keys():
        return "collection_event.coordinates."
    elif key in BOLDv4MediaItem.__fields__.keys():
        # this will be a list, so need to check all children
        return "specimen_imagery.media."
    elif key in BOLDv4Copyright.__fields__.keys():
        # this will be a list, so need to check all children
        return "specimen_imagery.media.copyright"
    elif key.replace("class", "classs") in BOLDv4Taxonomy.__fields__.keys():
        return "taxonomy."
    elif key.split("_")[-1] in BOLDv4Taxon.__fields__.keys():
        return f"taxonomy.{key.split('_')[0].replace('class', 'classs')}"
    else:
        raise ValueError("Could not resolve key to BOLD field.")


def rgetattr(obj: BOLDv4Result, attr: str, *args: str) -> Any | None:
    def _getattr(obj: BOLDv4Result, attr: str):
        return getattr(obj, attr, *args)

    return functools.reduce(_getattr, [obj] + attr.split("."))
