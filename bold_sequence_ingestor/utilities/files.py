from hashlib import md5

from pydantic import HttpUrl

from utilities.json import pretty_json


async def get_hash(call: HttpUrl):
    return md5(call.encode()).hexdigest()


async def write_to_file(name: str, fileFormat: str, content: bytes):
    filename = f"{name}.{fileFormat}"
    with open(filename, "wb") as file:
        file.write(content)


async def write_bold_data(fileFormat, call, content):
    if fileFormat == "json":
        content = await pretty_json(content)
    await write_to_file(
        name=await get_hash(call),
        fileFormat=fileFormat,
        # content=json.loads(content.decode("utf8").replace("'", '"')),
        content=content,
    )
