import json


async def decode_json(content: bytes) -> str:
    return json.loads(content.decode("utf8").replace("'", '"'))


async def pretty_json(content: bytes, encoding: str = "utf8") -> bytes:
    json_data = await decode_json(content)
    return json.dumps(json_data, indent=2).encode(encoding)
