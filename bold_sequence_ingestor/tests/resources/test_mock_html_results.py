import pytest

from tests.fixtures.mock_html_results import (
    mock_boldv3_url_taxonpage_result,
    mock_boldv4_url_taxonpage_result,
    mock_boldv3_url_recordview_result,
    mock_boldv4_url_recordview_result,
)

################################################################################
# TEST MOCK mock_boldv3_url_taxonpage_result FIXTURE
################################################################################
@pytest.mark.asyncio
async def test_mock_boldv3_url_taxonpage_result(
    mock_boldv3_url_taxonpage_result,
) -> None:
    html_contents = await mock_boldv3_url_taxonpage_result
    assert "Opharus bimaculata {species}</title>" in html_contents


################################################################################
# TEST MOCK mock_boldv4_url_taxonpage_result FIXTURE
################################################################################
@pytest.mark.asyncio
async def test_mock_boldv4_url_taxonpage_result(
    mock_boldv4_url_taxonpage_result,
) -> None:
    html_contents = await mock_boldv4_url_taxonpage_result
    assert "<title>Opharus bimaculata | Taxonomy Browser | " in html_contents


################################################################################
# TEST MOCK mock_boldv3_url_recordview_result FIXTURE
################################################################################


@pytest.mark.asyncio
async def test_mock_boldv3_url_recordview_result(
    mock_boldv3_url_recordview_result,
) -> None:
    html_contents = await mock_boldv3_url_recordview_result
    assert "<em>Opharus bimaculata</em>" in html_contents


################################################################################
# TEST MOCK mock_boldv4_url_recordview_result FIXTURE
################################################################################


@pytest.mark.asyncio
async def test_mock_boldv4_url_recordview_result(
    mock_boldv4_url_recordview_result,
) -> None:
    html_contents = await mock_boldv4_url_recordview_result
    assert "<em>Opharus bimaculata</em>" in html_contents
