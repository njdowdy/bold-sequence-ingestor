from cgitb import html
import pytest
import aiofiles

################################################################################
# CONSTRUCT MOCK TEST FIXTURE
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def mock_boldv3_url_taxonpage_result() -> str:
    """Fixture that returns mocked BOLD v3 TaxonPage
    e.g., https://v3.boldsystems.org/index.php/Taxbrowser_Taxonpage?taxid=97401
    """
    results = []
    async with aiofiles.open(
        "tests/resources/url_boldv3_taxonpage.html", mode="r", encoding="utf-8"
    ) as f:
        html_contents = await f.read()
        return html_contents


################################################################################
# CONSTRUCT MOCK TEST FIXTURE
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def mock_boldv4_url_taxonpage_result() -> str:
    """Fixture that returns mocked BOLD v4 TaxonPage
    e.g., https://boldsystems.org/index.php/TaxBrowser_TaxonPage?taxid=97401
    """
    async with aiofiles.open(
        "tests/resources/url_boldv4_taxonpage.html", mode="r", encoding="utf-8"
    ) as f:
        html_contents = await f.read()
        return html_contents


################################################################################
# CONSTRUCT MOCK TEST FIXTURE
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def mock_boldv3_url_recordview_result() -> str:
    """Fixture that returns mocked BOLD v4 Public RecordView
    e.g.,https://v3.boldsystems.org/index.php/Public_RecordView?processid=ARCTA366-07
    """
    results = []
    async with aiofiles.open(
        "tests/resources/url_boldv3_recordview.html", mode="r", encoding="utf-8"
    ) as f:
        html_contents = await f.read()
        return html_contents


################################################################################
# CONSTRUCT MOCK TEST FIXTURE
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def mock_boldv4_url_recordview_result() -> str:
    """Fixture that returns mocked BOLD v4 Public RecordView
    e.g.,https://boldsystems.org/index.php/Public_RecordView?processid=ARCTA366-07
    """
    results = []
    async with aiofiles.open(
        "tests/resources/url_boldv4_recordview.html", mode="r", encoding="utf-8"
    ) as f:
        html_contents = await f.read()
        return html_contents
