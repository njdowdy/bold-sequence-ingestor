import json

import aiofiles
import pytest

from models.bold4 import BOLDv4Result

################################################################################
# CONSTRUCT MOCK TEST FIXTURE
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def mock_bold_result_list() -> list[BOLDv4Result]:
    """Fixture that returns mocked BOLDv4 json data"""
    results = []
    async with aiofiles.open(
        "tests/resources/results.json", mode="r", encoding="utf-8"
    ) as f:
        contents = await f.read()
        json_content = dict(json.loads(contents))
        # return await asyncio.gather(
        #     *[BOLDv4Result(**value) for _, value in json_content.items()]
        # ) # not working "unhashable type BOLDv4Result"
        for _, value in json_content.items():
            results.append(BOLDv4Result(**value))
        return results


################################################################################
# CONSTRUCT MOCK TEST FIXTURE
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def mock_bold_result_singleton() -> BOLDv4Result:
    """Fixture that returns mocked BOLDv4Result object"""
    async with aiofiles.open(
        "tests/resources/result.json", mode="r", encoding="utf-8"
    ) as f:
        contents = await f.read()
        json_content = dict(json.loads(contents))
        for _, value in json_content.items():
            result = BOLDv4Result(**value)
        if result:
            return result
        else:
            raise ValueError("Could not be parsed")


################################################################################
# CONSTRUCT MOCK SEARCH PARAMETERS
################################################################################


@pytest.fixture
@pytest.mark.asyncio
async def search_params() -> dict[str, str]:
    return {
        "taxa": ["Viviennea", "Phaegoptera"],
        "geo": ["Costa Rica|Nicaragua", "Brazil|Peru"],
    }
