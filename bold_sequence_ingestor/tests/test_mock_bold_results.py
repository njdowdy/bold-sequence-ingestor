import pytest
from pytest import FixtureRequest

from models.bold4 import BOLDv4Result
from tests.fixtures.mock_bold_results import (
    mock_bold_result_list,
    mock_bold_result_singleton,
)

################################################################################
# TEST MOCK TEST FIXTURE
################################################################################


@pytest.mark.asyncio
async def test_mock_bold_result(mock_bold_result_list) -> None:
    result = await mock_bold_result_list
    assert len(result) == 20


################################################################################
# TEST MOCK TEST FIXTURE
################################################################################


@pytest.mark.asyncio
async def test_mock_bold_result_singleton(mock_bold_result_singleton) -> None:
    result = await mock_bold_result_singleton
    assert isinstance(result, BOLDv4Result)


################################################################################
# TEST MOCK SINGLETON FixtureRequest
################################################################################


@pytest.mark.parametrize(
    "fixture_name",
    [("mock_bold_result_singleton")],
)
@pytest.mark.asyncio
async def test_mock_bold_result_singleton_fixturerequest(
    fixture_name: str,
    request: FixtureRequest,
) -> None:
    fixture = await request.getfixturevalue(fixture_name)
    assert isinstance(fixture, BOLDv4Result)


################################################################################
# TEST MOCK LIST FixtureRequest
################################################################################


@pytest.mark.parametrize(
    "fixture_name",
    [("mock_bold_result_list")],
)
@pytest.mark.asyncio
async def test_mock_bold_result_list_fixturerequest(
    fixture_name: str,
    request: FixtureRequest,
) -> None:
    fixtures = await request.getfixturevalue(fixture_name)
    assert all([isinstance(fixture, BOLDv4Result) for fixture in fixtures])
