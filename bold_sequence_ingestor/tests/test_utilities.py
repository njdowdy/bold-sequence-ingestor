import pytest
from pytest import FixtureRequest

from utilities.general import rgetattr
from tests.fixtures.mock_bold_results import mock_bold_result_singleton

################################################################################
# TEST rgetattr FUNCTION
################################################################################


@pytest.mark.parametrize(
    "obj, attr, expected_output",
    [
        ("mock_bold_result_singleton", "specimen_desc.reproduction", "S"),
    ],
)
@pytest.mark.asyncio
async def test_rgetattr(
    obj: str, attr: str, expected_output: str, request: FixtureRequest
) -> None:
    obj = await request.getfixturevalue(obj)
    result = rgetattr(obj=obj, attr=attr)
    assert result == expected_output
