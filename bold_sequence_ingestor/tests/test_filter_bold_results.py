import pytest
from pytest import FixtureRequest

from functions.filter_bold_results import (
    compare_value,
    compose_parent_attribute_chain,
    convert_string_to_regex,
    evaluate_conditional,
    filter_results,
    fix_leading_trailing_space,
    fix_missing_leading_zero,
    format_search_param,
    is_existence_value,
    is_string_value,
    parse_search_params,
    sanitize_obj_value,
    stringify,
)
from tests.fixtures.mock_bold_results import mock_bold_result_list

################################################################################
# TEST compose_parent_attribute_chain FUNCTION
################################################################################


@pytest.mark.parametrize(
    "key, expected_output",
    [
        ("record_id", ""),
        ("processid", ""),
        ("bin_uri", ""),
        ("specimen_identifiers", ""),
        ("taxonomy", ""),
        ("specimen_desc", ""),
        ("collection_event", ""),
        ("specimen_imagery", ""),
        ("notes", ""),
        ("sampleid", "specimen_identifiers."),
        ("catalognum", "specimen_identifiers."),
        ("fieldnum", "specimen_identifiers."),
        ("institution_storing", "specimen_identifiers."),
        ("reproduction", "specimen_desc."),
        ("sex", "specimen_desc."),
        ("lifestage", "specimen_desc."),
        ("extra_info", "specimen_desc."),
        ("collectors", "collection_event."),
        ("country", "collection_event."),
        ("province_state", "collection_event."),
        ("region", "collection_event."),
        ("sector", "collection_event."),
        ("exactsite", "collection_event."),
        ("coordinates", "collection_event."),
        ("elev", "collection_event."),
        ("lat", "collection_event.coordinates."),
        ("lon", "collection_event.coordinates."),
        ("coord_source", "collection_event.coordinates."),
        ("coord_accuracy", "collection_event.coordinates."),
        ("mediaID", "specimen_imagery.media."),
        ("media_descriptor", "specimen_imagery.media."),
        ("copyright", "specimen_imagery.media."),
        ("photographer", "specimen_imagery.media."),
        ("image_file", "specimen_imagery.media."),
        ("copyright_holder", "specimen_imagery.media.copyright"),
        ("copyright_year", "specimen_imagery.media.copyright"),
        ("copyright_license", "specimen_imagery.media.copyright"),
        ("copyright_institution", "specimen_imagery.media.copyright"),
        ("identification_provided_by", "taxonomy."),
        ("phylum", "taxonomy."),
        ("class", "taxonomy."),
        ("order", "taxonomy."),
        ("family", "taxonomy."),
        ("subfamily", "taxonomy."),
        ("genus", "taxonomy."),
        ("species", "taxonomy."),
        ("phylum_taxID", "taxonomy.phylum"),
        ("class_taxID", "taxonomy.classs"),
        ("order_taxID", "taxonomy.order"),
        ("family_taxID", "taxonomy.family"),
        ("subfamily_taxID", "taxonomy.subfamily"),
        ("genus_taxID", "taxonomy.genus"),
        ("species_taxID", "taxonomy.species"),
        ("phylum_name", "taxonomy.phylum"),
        ("class_name", "taxonomy.classs"),
        ("order_name", "taxonomy.order"),
        ("family_name", "taxonomy.family"),
        ("subfamily_name", "taxonomy.subfamily"),
        ("genus_name", "taxonomy.genus"),
        ("species_name", "taxonomy.species"),
        ("phylum_reference", "taxonomy.phylum"),
        ("class_reference", "taxonomy.classs"),
        ("order_reference", "taxonomy.order"),
        ("family_reference", "taxonomy.family"),
        ("subfamily_reference", "taxonomy.subfamily"),
        ("genus_reference", "taxonomy.genus"),
        ("species_reference", "taxonomy.species"),
    ],
)
@pytest.mark.asyncio
async def test_compose_parent_attribute_chain(key: str, expected_output: str) -> None:
    result = await compose_parent_attribute_chain(key=key)
    assert result == expected_output


# could parameterize key str
@pytest.mark.asyncio
async def test_compose_parent_attribute_chain_fail() -> None:
    # "badstring" could be any non BOLDv4Result parameter
    with pytest.raises(ValueError):
        await compose_parent_attribute_chain(key="badstring")


################################################################################
# TEST is_existence_value FUNCTION
################################################################################


@pytest.mark.parametrize(
    "input, expected_output",
    [
        ("", True),
        ("!*", True),
        ("!", True),
        ("*", True),
        ("string!", False),
        ("string*", False),
        ("string!*", False),
        ("!string!", False),
        ("*string*", False),
        ("!*string!*", False),
        ("str!ing", False),
        ("str*ing", False),
        ("str!*ing", False),
    ],
)
@pytest.mark.asyncio
async def test_is_existence_value(input: str, expected_output: bool) -> None:
    result = is_existence_value(input=input)
    assert result == expected_output


################################################################################
# TEST format_search_param FUNCTION
################################################################################


@pytest.mark.parametrize(
    "search_param, expected_output",
    [
        ("''", "==''"),
        ("", "==None"),
        (" ", "==None"),
        ("!*", "==None"),
        ("!", "!=None"),
        ("*", "!=None"),
        ("*Test", "*Test"),
        ("*Test*", "*Test*"),
        ("Test*", "Test*"),
        ("Test", "=='Test'"),
        ("*.1", "*.1"),
        (".1*", "0.1*"),
        ("-.1*", "-0.1*"),
        ("-14", "==-14"),
        ("14", "==14"),
        ("==14", "==14"),
        (">14", ">14"),
        ("<14", "<14"),
        (">=14", ">=14"),
        ("<=14", "<=14"),
        (">-14", ">-14"),
        ("<-14", "<-14"),
        (">=-14", ">=-14"),
        ("<=-14", "<=-14"),
        ("9.1", "==9.1"),
        ("-9.1", "==-9.1"),
        ("==9.1", "==9.1"),
        (">9.1", ">9.1"),
        ("<9.1", "<9.1"),
        (">=9.1", ">=9.1"),
        ("<=9.1", "<=9.1"),
        (">-9.1", ">-9.1"),
        ("<-9.1", "<-9.1"),
        (">=-9.1", ">=-9.1"),
        ("<=-9.1", "<=-9.1"),
        (".1", "==0.1"),
        ("-.1", "==-0.1"),
        ("==.1", "==0.1"),
        (">.1", ">0.1"),
        ("<.1", "<0.1"),
        (">=.1", ">=0.1"),
        ("<=.1", "<=0.1"),
        (">-.1", ">-0.1"),
        ("<-.1", "<-0.1"),
        (">=-.1", ">=-0.1"),
        ("<=-.1", "<=-0.1"),
        ("F", "=='F'"),
        ("-F", "=='-F'"),
        (".F", "=='.F'"),
        (" 9", "==9"),
        ("> 9", ">9"),
        ("< 9", "<9"),
        (">= 9", ">=9"),
        ("<= 9", "<=9"),
        ("== 9", "==9"),
    ],
)
@pytest.mark.asyncio
async def test_format_search_param(search_param: str, expected_output: str) -> None:
    # changed to synchronous for now
    # result = await format_search_param(search_param=search_param)
    result = format_search_param(search_param=search_param)
    assert result == expected_output


################################################################################
# TEST parse_search_params FUNCTION
################################################################################


@pytest.mark.parametrize(
    "filter_value, expected_output",
    [
        (">9&<=13|!12&14", [[">9", "<=13"], ["!=12", "==14"]]),
        (">9", [[">9"]]),
        (">9|", [[">9"]]),
        ("|>9", [[">9"]]),
        ("|>9|", [[">9"]]),
        ("&>9", [[">9"]]),
        (">9&", [[">9"]]),
        ("&>9&", [[">9"]]),
        ("&>9|", [[">9"]]),
        ("|>9&", [[">9"]]),
        (">9&13", [[">9", "==13"]]),
        (">9&13|15", [[">9", "==13"], ["==15"]]),
        (">9&!13|!15", [[">9", "!=13"], ["!=15"]]),
        ("M", [["=='M'"]]),
        ("", [["==None"]]),
        ("''", [["==''"]]),
        ("None", [["=='None'"]]),
        ("!*", [["==None"]]),
        ("!", [["!=None"]]),
        ("*", [["!=None"]]),
        ("BOLD:AAM4813", [["=='BOLD:AAM4813'"]]),
    ],
)
@pytest.mark.asyncio
async def test_set_parse_search_params(
    filter_value: str, expected_output: list[list[str]]
) -> None:
    result = await parse_search_params(filter_value=filter_value)
    assert result == expected_output


################################################################################
# TEST is_string_value FUNCTION
################################################################################


@pytest.mark.parametrize(
    "input, expected_output",
    [
        ("S", True),
        ("Hello there", True),
        ("9_", True),
        (" 9", False),
        (".1", False),
        ("-7", False),
        (".e", True),
        (">9", False),
        (">=-9", False),
        ("ef9", True),
        ("9ef", True),
        ("9", False),
        ("09", False),
    ],
)
@pytest.mark.asyncio
async def test_is_string_value(
    input: str,
    expected_output: bool,
) -> None:
    result = is_string_value(input)
    assert result == expected_output


################################################################################
# TEST fix_leading_trailing_space FUNCTION
################################################################################


@pytest.mark.parametrize(
    "input, expected_output",
    [
        ("> 9", ">9"),
        (">= -.1", ">=-.1"),
        (" S", "S"),
        (" My String", "My String"),
        ("My String", "My String"),
    ],
)
@pytest.mark.asyncio
async def test_fix_leading_trailing_space(
    input: str,
    expected_output: bool,
) -> None:
    result = fix_leading_trailing_space(input)
    assert result == expected_output


################################################################################
# TEST fix_missing_leading_zero FUNCTION
################################################################################


@pytest.mark.parametrize(
    "input, expected_output",
    [
        ("==.1", "==0.1"),
        (">-.1", ">-0.1"),
        (">-.158", ">-0.158"),
        (">-0.1", ">-0.1"),
    ],
)
@pytest.mark.asyncio
async def test_fix_missing_leading_zero(
    input: str,
    expected_output: bool,
) -> None:
    result = fix_missing_leading_zero(input)
    assert result == expected_output


################################################################################
# TEST stringify FUNCTION
################################################################################


@pytest.mark.parametrize(
    "input, expected_output",
    [
        ("S", "'S'"),
        ("'S", "'S'"),
        ("S'", "'S'"),
        ("'S'", "'S'"),
        ("", "''"),
        ("''", "''"),
        ("None", "'None'"),
    ],
)
@pytest.mark.asyncio
async def test_stringify(
    input: str,
    expected_output: bool,
) -> None:
    result = stringify(input=input)
    assert result == expected_output


################################################################################
# TEST convert_string_to_regex FUNCTION
################################################################################


@pytest.mark.parametrize(
    "input, expected_output",
    [
        ("Test", "^Test$"),
        ("*Test", "^.+?Test$"),
        ("Test*", "^Test.+$"),
        ("*Test*", "^.+?Test.+$"),
    ],
)
@pytest.mark.asyncio
async def test_convert_string_to_regex(
    input: str,
    expected_output: str,
) -> None:
    result = convert_string_to_regex(input=input)
    assert result == expected_output


################################################################################
# TEST evaluate_conditional FUNCTION
################################################################################


@pytest.mark.parametrize(
    "obj_value, and_condition, expected_output",
    [
        ("S", "=='S'", True),
        ("Test", "=='Test'", True),
        ("My Test", "=='Test'", False),
        ("Test String", "=='Test'", False),
        ("My Test String", "=='Test'", False),
        ("My Test", "*Test", True),
        ("Test String", "*Test", False),
        ("My Test String", "*Test", False),
        ("My Test", "Test*", False),
        ("Test String", "Test*", True),
        ("My Test String", "Test*", False),
        ("My Test", "*Test*", False),
        ("Test String", "*Test*", False),
        ("My Test String", "*Test*", True),
        ("9.1", ">=9", True),
        ("9.1", ">9", True),
        ("9.1", "<9", False),
        ("9.1", "<=9", False),
        ("9", "<9", False),
        ("9", ">9", False),
        ("9", "==9", True),
        ("9.0", "<9.0", False),
        ("9.0", ">9.0", False),
        ("9.0", "==9.0", True),
        ("-9", "==-9", True),
        ("-9.0", "==-9.0", True),
        ("-9.1", "<-9", True),
        ("-9.1", ">-9", False),
        ("-9.1", "<=-9", True),
        ("-9.1", ">=-9", False),
        (None, "==None", True),
    ],
)
@pytest.mark.asyncio
async def test_evaluate_conditional(
    obj_value: str | None,
    and_condition: str,
    expected_output: bool,
) -> None:
    result = evaluate_conditional(obj_value=obj_value, and_condition=and_condition)
    assert result == expected_output


################################################################################
# TEST sanitize_obj_value FUNCTION
################################################################################


@pytest.mark.parametrize(
    "obj_value, expected_output",
    [
        ("", ""),
        ("''", ""),
        ("'Test' 'string'", "Test string"),
    ],
)
@pytest.mark.asyncio
async def test_sanitize_obj_value(
    obj_value: str,
    expected_output: bool,
) -> None:
    result = sanitize_obj_value(obj_value=obj_value)
    assert result == expected_output


################################################################################
# TEST compare_value FUNCTION
################################################################################


@pytest.mark.parametrize(
    "obj_value, filter_values, exact_mode, expected_output",
    [
        ("S", [["=='M'"], ["=='F'"]], False, False),
        ("My Test", [["=='Test'"]], False, False),
        ("Test String", [["=='Test'"]], False, False),
        ("My Test String", [["=='Test'"]], False, False),
        ("My Test", [["*Test"]], False, True),
        ("Test String", [["*Test"]], False, False),
        ("My Test String", [["*Test"]], False, False),
        ("My Test", [["Test*"]], False, False),
        ("Test String", [["Test*"]], False, True),
        ("My Test String", [["Test*"]], False, False),
        ("My Test", [["*Test*"]], False, False),
        ("Test String", [["*Test*"]], False, False),
        ("My Test String", [["*Test*"]], False, True),
    ],
)
@pytest.mark.asyncio
async def test_compare_value(
    obj_value: str,
    filter_values: list[list[str]],
    exact_mode: bool,
    expected_output: bool,
) -> None:
    result = compare_value(
        obj_value=obj_value, filter_values=filter_values, exact_mode=exact_mode
    )
    assert result == expected_output


################################################################################
# TEST filter_results FUNCTION
################################################################################


@pytest.mark.parametrize(
    "results, exact_mode, kwargs, expected_filtered_length",
    [
        ("mock_bold_result_list", False, {"record_id": "1582194"}, 1),
        ("mock_bold_result_list", False, {"processid": "LEMMZ057-10"}, 1),
        ("mock_bold_result_list", False, {"bin_uri": "BOLD*"}, 20),
        ("mock_bold_result_list", False, {"bin_uri": "BOLD:AAM4813"}, 5),
        ("mock_bold_result_list", False, {"specimen_identifiers": ""}, 1),
        ("mock_bold_result_list", False, {"specimen_identifiers": "*"}, 19),
        ("mock_bold_result_list", False, {"specimen_desc": ""}, 0),
        ("mock_bold_result_list", False, {"specimen_desc": "*"}, 20),
        ("mock_bold_result_list", False, {"collection_event": ""}, 0),
        ("mock_bold_result_list", False, {"collection_event": "*"}, 20),
        # ("mock_bold_result_list", False, {"specimen_imagery": ""}, 5),
        # ("mock_bold_result_list", False, {"specimen_imagery": "*"}, 15),
        # ("mock_bold_result_list", False, {"copyright": ""}, 5),
        # ("mock_bold_result_list", False, {"copyright": "*"}, 15),
        ("mock_bold_result_list", False, {"notes": "''"}, 18),
        ("mock_bold_result_list", False, {"notes": "!*"}, 1),
        ("mock_bold_result_list", False, {"notes": "*"}, 19),
        ("mock_bold_result_list", False, {"notes": ""}, 1),
        ("mock_bold_result_list", False, {"taxonomy": "*"}, 20),
        ("mock_bold_result_list", False, {"taxonomy": "!"}, 20),
        ("mock_bold_result_list", False, {"taxonomy": "!*"}, 0),
        ("mock_bold_result_list", False, {"taxonomy": ""}, 0),
        ("mock_bold_result_list", False, {"sex": "M"}, 3),
        ("mock_bold_result_list", False, {"sex": ""}, 15),
        ("mock_bold_result_list", False, {"sex": "!*"}, 15),
        ("mock_bold_result_list", False, {"sex": "!"}, 5),
        ("mock_bold_result_list", False, {"sex": "*"}, 5),
        ("mock_bold_result_list", False, {"sex": "U"}, 1),
        # ("mock_bold_result_list", False, {"taxonomy": "*"}, 20),
    ],
)
@pytest.mark.asyncio
async def test_filter_results(
    results: str,
    exact_mode: bool,
    kwargs: dict[str, str],
    expected_filtered_length: int,
    request: FixtureRequest,
) -> None:

    results = await request.getfixturevalue(results)
    filtered_results = await filter_results(
        results=results, to_file=False, exact_mode=exact_mode, **kwargs
    )
    assert len(filtered_results) == expected_filtered_length


@pytest.mark.asyncio
async def test_filter_results_no_kwargs(mock_bold_result_list) -> None:
    results = await mock_bold_result_list
    filtered_results = await filter_results(
        results=results, to_file=False, exact_mode=False
    )
    assert filtered_results == results


@pytest.mark.parametrize(
    "results, exact_mode, kwargs",
    [
        (None, False, {"sex": "M"}),
    ],
)
@pytest.mark.asyncio
async def test_filter_results_no_results_list(
    results: None,
    exact_mode: bool,
    kwargs: dict[str, str],
) -> None:

    with pytest.raises(ValueError):
        await filter_results(
            results=results, to_file=False, exact_mode=exact_mode, **kwargs
        )
