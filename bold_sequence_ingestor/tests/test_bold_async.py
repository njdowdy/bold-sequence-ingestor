import pytest
from aiohttp import ClientSession

from functions.bold_async import pull_bold_data_async
from tests.fixtures.mock_bold_results import search_params

################################################################################
#  TEST API CONNECTION
################################################################################


@pytest.mark.asyncio
async def test_bold_server_response() -> None:
    base_url = "http://boldsystems.org/index.php/API_Public/specimen?"
    endpoint = base_url + "taxon=Opharus&geo=Costa%20Rica&format=tsv"
    async with ClientSession() as session:
        async with session.get(endpoint) as resp:
            try:
                assert resp.status == 200
                # if records are returned:
                # assert each record returned has taxon = Opharus
                # assert each record returned has country = Costa Rica
                # parameterize and test other api parameters too
                # test that nonsense returns nothing
            except resp.raise_for_status() as e:
                raise e  # this doesn't fail the test, it stops testing if raised


################################################################################
#  TEST pull_bold_data_async FUNCTION
################################################################################


@pytest.mark.asyncio
async def test_pull_bold_data_async(search_params: dict[str, str]) -> None:
    search_params = await search_params
    result = await pull_bold_data_async(
        taxa=search_params["taxa"], geo=search_params["geo"]
    )
    assert len(result) == 95
