import pytest
from pytest import FixtureRequest
from aiohttp import ClientSession

from tests.fixtures.mock_html_results import mock_boldv3_url_recordview_result
from tests.fixtures.mock_bold_results import mock_bold_result_list

from functions.url_scraper import get_boldv4result_field, parse_boldv3_recordview

################################################################################
#  TEST URL CONNECTION
################################################################################


@pytest.mark.asyncio
async def test_bold_url_response() -> None:
    base_url = "https://boldsystems.org/index.php/Public_RecordView?"
    endpoint = base_url + "processid=ARCTA366-07"
    async with ClientSession() as session:
        async with session.get(endpoint) as resp:
            try:
                assert resp.status == 200
            except resp.raise_for_status() as e:
                raise e  # this doesn't fail the test, it stops testing if raised


################################################################################
#  TEST parse_boldv3_taxonpage FUNCTION
################################################################################


# @pytest.mark.asyncio
# async def test_parse_boldv3_recordview(mock_boldv3_url_recordview_result) -> None:
#     results = await mock_boldv3_url_recordview_result
#     parsed_results = await parse_boldv3_recordview(results)
#     assert parsed_results == results


################################################################################
#  TEST parse_boldv4_taxonpage FUNCTION
################################################################################


################################################################################
#  TEST parse_boldv3_recordview FUNCTION
################################################################################


################################################################################
#  TEST parse_boldv4_recordview FUNCTION
################################################################################

################################################################################
#  TEST get_boldv4result_field FUNCTION
################################################################################


@pytest.mark.parametrize(
    "mock_obj, key, expected_data",
    [
        (
            "mock_bold_result_list",
            "processid",
            [
                "LEMMZ057-10",
                "LEMMZ058-10",
                "LEMMZ059-10",
                "LEMMZ093-10",
                "LEMMZ1192-12",
                "LEMMZ1193-12",
                "LEMMZ1194-12",
                "LEMMZ1195-12",
                "LEMMZ1196-12",
                "LEMMZ1232-12",
                "LEMMZ1234-12",
                "LEMMZ1235-12",
                "LEMMZ1308-12",
                "LEMMZ289-10",
                "LEMMZ684-11",
                "LEMMZ693-11",
                "LEMMZ771-11",
                "LEMMZ772-11",
                "LEMMZ773-11",
                "LEMMZ850-11",
            ],
        ),
    ],
)
@pytest.mark.asyncio
async def test_get_boldv4result_field(
    mock_obj: str,
    key: str,
    expected_data: list[str],
    request: FixtureRequest,
) -> None:
    result_list = await request.getfixturevalue(mock_obj)
    data = await get_boldv4result_field(result_list, key)
    assert data == expected_data
