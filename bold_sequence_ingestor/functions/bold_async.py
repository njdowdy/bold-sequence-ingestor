import asyncio
import json
import time
from http.client import HTTPException
from typing import Coroutine

import aiofiles
from aiofiles.threadpool.text import AsyncTextIOWrapper
from aiohttp import ClientResponse, ClientSession
from pydantic import HttpUrl

from functions.filter_bold_results import filter_results
from utilities.general import compose_api_call, compose_search
from utilities.files import get_hash
from utilities.json import pretty_json
from models.base import BOLDApiCall
from models.bold4 import BOLDv4MediaItem, BOLDv4Result


async def write_file_async(
    content: ClientResponse, filename: str, mode: str | None = "ba"
) -> None:
    # I think there's a problem writing malformed JSON here
    # (not a list and missing commas between each item)
    # possibly compounded by append mode?
    # it is just compounding the emitted call results,
    # rather than combining them into one results json
    if filename.split(".")[-1].lower() == "json":
        content = await content.read()
        content = await pretty_json(content, encoding="utf8")
    else:
        content = await content.text()
    async with aiofiles.open(f"outputs/{filename}", mode) as f:
        f: AsyncTextIOWrapper = f  # type hinting f
        await f.write(content)


async def write_response_async(
    response: ClientResponse, filename: str, mode: str | None = "ba"
):
    try:
        if response.status == 200:
            await write_file_async(response, filename)
        else:
            raise HTTPException
    except HTTPException as e:
        print(f"Bad Response: {e}")


async def make_request(
    session: ClientSession,
    url: BOLDApiCall,
    filename: str,
    to_file: bool | None = True,
) -> ClientResponse:
    try:
        response = await session.get(url.url)
        filename = filename
        if to_file:
            await write_response_async(response, filename)
        else:
            print(response)
        return response
    except BaseException as e:
        print(f"An unknown exception occurred: {e}")


async def compose_tasks(
    calls: list[BOLDApiCall],
    session: ClientSession,
    filenames: list[BOLDApiCall] | None = None,
    to_file: bool | None = True,
) -> list[Coroutine]:
    tasks = []
    try:
        if filenames:
            for i, call in enumerate(calls):
                tasks.append(make_request(session, call, filenames[i], to_file))
        else:
            raise ValueError("Filenames must be provided to write out results.")
    except ValueError as e:
        print(f"ValueError: {e}")
    return tasks


async def emit_many_calls(
    calls: list[tuple[str, BOLDApiCall]],
    combine_results: bool | None = False,
    to_file: bool | None = True,
    output_filename: str | None = "_results_combined.json",
    return_results: bool | None = True,
    # ) -> list[ClientResponse] | None:
) -> list[str] | None:
    filenames, urls = zip(*calls)
    if combine_results:
        filenames = [output_filename] * len(urls)
    async with ClientSession() as session:
        tasks = await compose_tasks(urls, session, filenames, to_file)
        responses = await asyncio.gather(*tasks)
        responses = await handle_html_responses(responses)  # new
    return responses


async def handle_html_responses(responses: list[ClientResponse]) -> list[str]:
    results = []
    for response in responses:
        if response.status == 200 and "text" in response.content_type:
            result = await response.text()
            results.append(result)
    return results


async def handle_json_responses(responses: list[ClientResponse]) -> list[BOLDv4Result]:
    results = []
    for response in responses:
        if response.status == 200 and "json" in response.content_type:  # new
            result: bytes = await response.read()  # TODO: change to .json()?
            result_json: dict = json.loads(result.decode())["bold_records"]["records"]
            for _, value in result_json.items():
                results.append(BOLDv4Result(**value))
    return results


def generate_filename(record: BOLDv4Result, media_record: BOLDv4MediaItem) -> str:
    process_id = record.processid if record.processid else ""
    bin_uri = record.bin_uri.replace("BOLD:", "") if record.bin_uri else ""
    record_id = record.record_id if record.record_id else ""
    country = (
        record.collection_event.country
        if record.collection_event.country
        else "Unspecified"
    )
    sample_id = (
        record.specimen_identifiers.sampleid
        if record.specimen_identifiers.sampleid
        else ""
    )
    if record.taxonomy.species:
        name = record.taxonomy.species["taxon"].name.replace(" ", "_")
    elif record.taxonomy.genus:
        name = record.taxonomy.genus["taxon"].name
    elif record.taxonomy.subfamily:
        name = record.taxonomy.subfamily["taxon"].name
    else:
        name = "Unspecified_Taxon"
    media_id = media_record.mediaID if media_record.mediaID else ""
    specimen_orientation = (
        media_record.media_descriptor.lower() if media_record.media_descriptor else ""
    )
    file_extension = media_record.image_file.split(".")[-1]
    sample_info = f"{process_id}_{bin_uri}_{record_id}_{sample_id}_{name}_{country}_"
    media_info = f"{media_id}_{specimen_orientation}.{file_extension}"
    return f"{sample_info + media_info}".replace("__", "_")


async def generate_calls(items: list[BOLDv4Result]) -> list[tuple[str, HttpUrl]]:
    # I couldn't figure out async on generate_filename
    # probably need compose_task and asyncio.gather
    calls = [
        item
        for sublist in [
            [(generate_filename(x, y), y.image_file) for y in x.specimen_imagery.media]
            for x in items
        ]
        for item in sublist
    ]
    return calls


async def compose_kwargs(
    taxa: list[str], **kwargs: list[str]
) -> dict[str, dict[str, str]]:
    data = {}
    for i, taxon in enumerate(taxa):
        values = {}
        [values.update({f"{x}": kwargs[f"{x}"][i].split("|")}) for x in kwargs]
        values = {k: v for k, v in values.items() if v}
        data[f"{taxon}"] = values
    return data


async def pull_bold_images_async(
    items: list[BOLDv4Result],
    max_results: int | None = None,
    timeit: bool | None = True,
) -> None:
    # TODO: add a filter on images here (e.g., "media_descriptor=Dorsal")
    # this filtering needs to be done after the expansion from records to media items
    # (and then removed from list of media items before pulling images)
    if timeit:
        start = time.perf_counter()
    records_with_images = await filter_results(
        items,
    )
    if timeit:
        chkpt1 = time.perf_counter()
    if records_with_images:
        calls = await generate_calls(records_with_images)
        if timeit:
            chkpt2 = time.perf_counter()
        if max_results and max_results < len(calls):
            calls = calls[0:max_results]
        res = await emit_many_calls(
            calls,
            combine_results=False,
            to_file=True,
            return_results=False,
        )
        await handle_json_responses(res)
        if timeit:
            chkpt3 = time.perf_counter()
        print(
            f"have-images: {chkpt1-start}\n\
                generate-calls: {chkpt2-chkpt1}\n\
                results: {chkpt3-chkpt2}\n\
                total: {chkpt3-start}\n"
        )
    else:
        print("No images to retrieve.")


async def pull_bold_data_async(
    taxa: list[str],
    timeit: bool | None = True,
    to_file: bool | None = True,
    output_filename: str | None = "_results_combined.json",
    **kwargs: list[str] | None,
) -> list[BOLDv4Result]:
    if timeit:
        start = time.perf_counter()
    data = await compose_kwargs(taxa, **kwargs)
    if timeit:
        chkpt1 = time.perf_counter()
    searches = [
        (await compose_search(taxon=[key], **values)) for key, values in data.items()
    ]
    if timeit:
        chkpt2 = time.perf_counter()
    api_calls = [await compose_api_call(search) for search in searches]
    if timeit:
        chkpt3 = time.perf_counter()
    calls = [((await get_hash(call.url)), call) for call in api_calls]
    if timeit:
        chkpt4 = time.perf_counter()
    result = await emit_many_calls(
        calls, combine_results=True, to_file=to_file, output_filename=output_filename
    )
    result = await handle_json_responses(result)
    if timeit:
        chkpt5 = time.perf_counter()
        # final_result = await filter_results()
        # if timeit:
        #     chkpt6 = time.perf_counter()
        print(
            f"data: {chkpt1-start}\n\
                searches: {chkpt2-chkpt1}\n\
                apicalls: {chkpt3-chkpt2}\n\
                calls: {chkpt4-chkpt3}\n\
                result: {chkpt5-chkpt4}\n\
                total: {chkpt5-start}\n"
        )
    return result
