import re
from typing import Any

from utilities.general import rgetattr, compose_parent_attribute_chain
from models.bold4 import (
    BOLDv4Result,
)


def is_string_value(input: str) -> bool:
    if re.match(
        r"^$|^[\s\*!-\.<>=0-9]+$|^\*[A-z][A-z0-9]*\**$|^[A-z][A-z0-9]*\*$", input
    ):
        return False
    else:
        return True


def stringify(input: str) -> str:
    if input == "":
        input = "''"
    if is_string_value(input):
        if input[0] != "'":
            input = "'" + input
        if input[-1] != "'":
            input = input + "'"
    return input


def fix_leading_trailing_space(input: str) -> str:
    # remove leading space
    input = re.sub(r"^(|==|>|<|<=|>=|==-|>-|<-|<=-|>=-)[\s]", "\\1", input)
    # remove trailing space
    input = re.sub(r"\s+$", "", input)
    return input


def fix_missing_leading_zero(input: str) -> str:
    # fix missing leading zero
    random_str = "```"
    return re.sub(
        r"^(|==|>|<|<=|>=|==-|>-|<-|<=-|>=-|-)[\.\s](\d|\*+$)",
        f"\\1{random_str}0.\\2",
        input,
    ).replace(f"{random_str}", "")


def is_existence_value(input: str) -> bool:
    empty = re.match(r"^$|^==None$", input)
    not_anything = re.match(r"^!\*$|^==None$", input)
    not_nothing = re.match(r"^!$|^!=None$", input)
    anything = re.match(r"^\*$|^!=None$", input)
    if any([empty, not_anything, not_nothing, anything]):
        return True
    else:
        return False


# changed to synchronous for now
# async def format_search_param(search_param: str) -> str:
def format_search_param(search_param: str) -> str:
    # fix malformed string values
    search_param = fix_leading_trailing_space(search_param)
    search_param = fix_missing_leading_zero(search_param)
    if is_existence_value(search_param):
        search_param = re.sub(r"^!$", "!=None", search_param)
        search_param = re.sub(r"^\*$", "!=None", search_param)
        search_param = re.sub(r"^$", "==None", search_param)
        search_param = re.sub(r"^!\*$", "==None", search_param)
    else:
        search_param = re.sub(r"^!", "!=", search_param)
        # check for missing leading '=='
        is_raw_value = re.match(r"^[0-9A-z\-\.':]+$", search_param)
        # put quotes around strings
        search_param = stringify(search_param)
        # add missing leading '=='
        if is_raw_value:
            search_param = "==" + search_param
    return search_param


async def parse_search_params(filter_value: str) -> list[list[str]]:
    # remove unnecessary leading/trailing '&' and '|'
    filter_value = re.sub(r"^&|^\|", "", filter_value)
    filter_value = re.sub(r"&$|\|$", "", filter_value)
    # create groups of or conditions
    or_groups = filter_value.split("|")
    # create subgroups of and conditions and sanitize them
    filter_values = [
        [
            # await format_search_param(and_condition) # changed to synchronous for now
            format_search_param(and_condition)
            for and_condition in and_group.split("&")
        ]
        for and_group in or_groups
    ]
    return filter_values


def convert_string_to_regex(input: str) -> str:
    regex_string = re.sub(r"^\*", "^.+?", input)
    regex_string = re.sub(r"\*$", ".+$", regex_string)
    if regex_string[0] != "^":
        regex_string = "^" + regex_string
    if regex_string[-1] != "$":
        regex_string = regex_string + "$"
    return regex_string


def sanitize_obj_value(obj_value: str) -> str:
    return obj_value.replace("'", "")


def evaluate_conditional(obj_value: str | None, and_condition: str) -> bool:
    is_regex = True if re.match(r"^\*|.*\*$", and_condition) else False
    if not is_regex:
        # sanitize obj_values containing expressions
        obj_value = sanitize_obj_value(obj_value) if obj_value is not None else None
        # put quotes around strings
        obj_value = stringify(obj_value) if obj_value is not None else None
        return eval(f"{obj_value}{and_condition}")
    else:
        regex_search_result = re.match(
            rf"{convert_string_to_regex(and_condition)}", obj_value
        )
        if regex_search_result:
            return True
        else:
            return False


def compare_value(
    obj_value: Any | None, filter_values: list[list[str]], exact_mode: bool
) -> bool:

    if obj_value:
        obj_value = str(obj_value)
    if obj_value and not exact_mode:
        obj_value = obj_value.lower()
        filter_values = [
            [
                and_conditions.lower()
                if not is_existence_value(and_conditions)
                else and_conditions
                for and_conditions in or_conditions
            ]
            for or_conditions in filter_values
        ]
    comparison_result = any(
        [
            all(
                [
                    evaluate_conditional(obj_value, and_condition)
                    for and_condition in or_group
                ]
            )
            for or_group in filter_values
        ]
    )
    return comparison_result


# def extract_result_value(
#     bold_result: BOLDv4Result, parent_attrs: str, key: str
# ) -> Any | None:
#     checks = []
#     attr_list = parent_attrs.rstrip(".").split(".")
#     for i, _ in enumerate(attr_list):
#         attr = ".".join(attr_list[0 : i + 1])
#         try:
#             rgetattr(bold_result, f"{attr}")
#             checks.append(True)
#         except:
#             pass
#         else:
#             checks.append(False)
#     parent_hierarchy_exists = True if all(checks) else False
#     if parent_hierarchy_exists:
#         return rgetattr(bold_result, f"{parent_attrs}{key}")
#     else:
#         return None


async def filter_results(
    results: list[BOLDv4Result],
    to_file: bool | None = False,
    output_filename: str | None = "_filtered_results.json",
    exact_mode: bool | None = False,
    **kwargs: str,
) -> list[BOLDv4Result]:
    """Filters a list of BOLD API v4 results based on input parameters.

    \nThe results must be cast to a list of BOLDv4Result objects before filtering.
    \nInput parameters should be given as quoted strings, even if they are integers.
    \nWildcards are supported:
    - "*test*" matches "My test" and "test string"
    - "*test" only matches "My test"
    - "test*" only matches "test string").
    \nLogical operators are supported including 'AND' (&), 'OR' (|), 'NOT' (!):
    - ">=-9&<=13&!12|100" will match values between 13 and -9 except 12, as well as 100
    \nExistence operations are supported:
    - "*" = "Anything", any value except None
    - "!*" = "Not Anything", only None
    - "" = "Nothing", only None
    - "!" = "Not Nothing", any value except None
    \nTo match an field containing an explicit empty string, pass the value:
    \n" ' ' " (double-quoted single-quotes, without spaces).

    \nPossible keyword arguments are described below.
    \nIf no valid entries are passed, no filtering will be applied.

    Args:
        results (list[BOLDv4Result]): List of API results cast to a list of BOLDv4Result objects.
        to_file (bool | None, optional): Flag to send results to an output file. Defaults to False.
        output_filename (str | None, optional): Results output filename. Defaults to "_filtered_results.json".
        exact_mode (bool | None, optional): Flag to match cases exactly. Defaults to False.

    Keyword Args:
        record_id (str): description
        processid (str): description
        bin_uri (str): Barcode Index Number URI (e.g., "BOLD:AAA5125")
        specimen_identifiers (str): description
        taxonomy (str): description
        specimen_desc (str): description
        collection_event (str): description
        specimen_imagery (str): description
        notes (str): description
        sampleid (str): description
        catalognum (str): description
        fieldnum (str): description
        institution_storing (str): description
        reproduction (str): description
        sex (str): description
        lifestage (str): description
        extra_info (str): description
        collectors (str): description
        country (str): description
        province_state (str): description
        region (str): description
        sector (str): description
        exactsite (str): description
        coordinates (str): description
        elev (str): description
        lat (str): description
        lon (str): description
        coord_source (str): description
        coord_accuracy (str): description
        mediaID (str): description
        media_descriptor (str): description
        copyright (str): description
        photographer (str): description
        image_file (str): description
        copyright_holder (str): description
        copyright_year (str): description
        copyright_license (str): description
        copyright_institution (str): description
        identification_provided_by (str): description
        phylum (str): description
        class (str): description
        order (str): description
        family (str): description
        subfamily (str): description
        genus (str): description
        species (str): description
        phylum_taxID (str): description
        class_taxID (str): description
        order_taxID (str): description
        family_taxID (str): description
        subfamily_taxID (str): description
        genus_taxID (str): description
        species_taxID (str): description
        phylum_name (str): description
        class_name (str): description
        order_name (str): description
        family_name (str): description
        subfamily_name (str): description
        genus_name (str): description
        species_name (str): description
        phylum_reference (str): description
        class_reference (str): description
        order_reference (str): description
        family_reference (str): description
        subfamily_reference (str): description
        genus_reference (str): description
        species_reference (str): description

    Raises:
        ValueError: No results were provided to filter.

    Returns:
        list[BOLDv4Result]: A filtered list of BOLD API v4 results.
    """
    if not results:
        raise ValueError("No results to filter.")
    else:
        filtered_results = results
    for key, filter_value in kwargs.items():
        parent_attrs = await compose_parent_attribute_chain(key)
        filter_values = await parse_search_params(str(filter_value))
        filtered_results = list(
            filter(
                lambda bold_result: compare_value(
                    # obj_value=extract_result_value(
                    #     bold_result=bold_result, parent_attrs=parent_attrs, key=key
                    # ),
                    obj_value=rgetattr(
                        bold_result, f"{parent_attrs}{key}"
                    ),  # TODO: doesn't work if parent hierarchy missing
                    filter_values=filter_values,
                    exact_mode=exact_mode,
                ),
                results,
            )
        )
    return filtered_results
