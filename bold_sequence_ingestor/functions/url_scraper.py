import re
from bs4 import BeautifulSoup as soup, Tag, ResultSet
from models.bold4 import BOLDv4Result
from models.base import BOLDApiCall
from models.bold_recordview import (
    BOLDv4RecordView,
    BOLDv4RecordViewTaxonomy,
    BOLDv4RecordViewAttribution,
    BOLDv4RecordViewCollectingEvent,
    BOLDv4RecordViewElectropherogram,
    BOLDv4RecordViewIdentifiers,
    BOLDv4RecordViewSequenceInfo,
    BOLDv4RecordViewSpecimenDesc,
)

from functions.bold_async import emit_many_calls
from utilities.general import rgetattr, compose_parent_attribute_chain

# "https://boldsystems.org/index.php/Public_RecordView?processid=ARCTA366-07"


async def get_boldv4result_field(
    result_list: list[BOLDv4Result],
    key: str
    # with fresher info
) -> list[str]:
    parent_attrs = await compose_parent_attribute_chain(key)
    data = [
        rgetattr(x, f"{parent_attrs}{key}") for x in result_list
    ]  # TODO: doesn't work if parent hierarchy missing for x in result_list]
    return data


async def generate_url_list(
    base_url: str, results_list: list[BOLDv4Result]
) -> list[tuple[str, BOLDApiCall]]:
    # attach the pull down date to result sets for generating DOIs
    # for result sets, really need a database that saves alternative
    # data states for each field with a date when data was replaced
    processids = await get_boldv4result_field(results_list, "processid")
    calls = [(i, BOLDApiCall(url=f"{base_url}{i}")) for i in processids]
    return calls


async def fetch_html(
    calls: list[tuple[str, BOLDApiCall]], to_file: bool = False
) -> list[str]:
    responses = await emit_many_calls(calls=calls, to_file=to_file)
    return responses


async def parse_html_table_data(html: soup, search: str) -> dict[ResultSet]:
    table_row1: Tag = html.find(text=re.compile(f"{search}")).findParent()
    while table_row1.findParent().findAll("tr") == []:
        table_row1 = table_row1.findParent()
    results = {}
    results["table_row1_data"] = table_row1.findAll("td")
    siblings = table_row1.find_next_siblings()
    for row, _ in enumerate(siblings):
        current_row: Tag = siblings[row]
        # if current_row.findAll("td") doesn't exist, we won't have a ".text" to access later!
        results[f"table_row{row+2}_data"] = current_row.findAll("td")
    return results


async def parse_boldv3_recordview(html_pages: list[str]) -> list[BOLDv4RecordView]:
    parsed_results = []
    async for html in html_pages:
        data = soup(html, "html.parser")
        # cast into appropriate model
        table_data = await parse_html_table_data(data, "Sample ID")
        identifiers = BOLDv4RecordViewIdentifiers(
            sampleid=table_data.get("table_row1_data")[1].text,
            fieldnum=table_data.get("table_row2_data")[1].text,
            institution_storing=table_data.get("table_row3_data")[1].text,
            museumid=table_data.get("table_row1_data")[3].text,
            collection_code=table_data.get("table_row2_data")[3].text,
        )
        table_data = await parse_html_table_data(data, "Phylum")
        taxonomy = BOLDv4RecordViewTaxonomy(
            phylum=table_data.get("table_row1_data")[1].text,
            classs=table_data.get("table_row2_data")[1].text,
            order=table_data.get("table_row3_data")[1].text,
            family=table_data.get("table_row4_data")[1].text,
            subfamily=table_data.get("table_row1_data")[3].text,
            genus=table_data.get("table_row2_data")[3].text,
            species=table_data.get("table_row3_data")[3].text,
            subspecies=table_data.get("table_row4_data")[3].text,
            bin_uri=table_data.get("table_row5_data")[1].text.strip(),
        )
        table_data = await parse_html_table_data(data, "Voucher Status")
        specimen_details = BOLDv4RecordViewSpecimenDesc(
            voucher_status=table_data.get("table_row1_data")[1].text,
            reproduction=table_data.get("table_row1_data")[3].text,
            sex=table_data.get("table_row2_data")[3].text,
            lifestage=table_data.get("table_row3_data")[3].text,
            tissue_descriptor=table_data.get("table_row2_data")[1].text,
            brief_note=table_data.get("table_row3_data")[1].text,
            detailed_notes=table_data.get("table_row4_data")[1].text,
        )
        table_data = await parse_html_table_data(data, "Country")
        collection_details = BOLDv4RecordViewCollectingEvent(
            collectors=table_data.get("table_row2_data")[3].text,
            date_collected=table_data.get("table_row1_data")[3].text,
            country=table_data.get("table_row1_data")[1].text,
            province_state=table_data.get("table_row2_data")[1].text,
            region_county=table_data.get("table_row3_data")[1].text,
            sector=table_data.get("table_row4_data")[1].text,
            exactsite=table_data.get("table_row5_data")[1].text,
            lat=table_data.get("table_row6_data")[1].text,
            lon=table_data.get("table_row7_data")[1].text,
            coord_source=table_data.get("table_row8_data")[1].text,
            coord_accuracy=table_data.get("table_row9_data")[1].text,
            elev=table_data.get("table_row6_data")[3].text.replace(" Meters", ""),
            elev_accuracy=table_data.get("table_row7_data")[3].text,
            depth=table_data.get("table_row8_data")[3].text.replace(" Meters", ""),
            depth_accuracy=table_data.get("table_row9_data")[3].text,
        )
        table_data = await parse_html_table_data(data, "Sequence ID")
        sequence_coi5p = BOLDv4RecordViewSequenceInfo(
            funding_source=data.find(text=re.compile("SEQUENCE: COI-5P"))
            .split(" ")[-1]
            .replace("]", ""),
            sequence_id=table_data.get("table_row1_data")[1].text,
            last_updated=table_data.get("table_row2_data")[1].text.strip(),
            genbank_accession=(
                table_data.get("table_row1_data")[3].text.strip(),
                table_data.get("table_row1_data")[3].find("a")["href"],
            ),
            genome=table_data.get("table_row2_data")[3].text,
            locus=table_data.get("table_row3_data")[1].text,
            nucleotides=table_data.get("table_row4_data")[1].text.replace("bp", ""),
            dna_sequence=table_data.get("table_row5_data")[0].text.strip(),
            aa_sequence=table_data.get("table_row7_data").text.strip(),
        )
        table_data = await parse_html_table_data(data, "Length")
        electropherogram_traces_coi5p = BOLDv4RecordViewElectropherogram(
            length=0,
            pcr_primers="",
            read_direction="",
            status="",
            run_date="",
            link="",
        )
        table_data = await parse_html_table_data(data, "Specimen Depository")
        attribution = BOLDv4RecordViewAttribution(
            specimen_depository="",
            sequencing_center="",
            photography="",
            collectors="",
            specimen_identification="",
            project_manager="",
            sequencing_support="",
        )
        result = BOLDv4RecordView(
            specimen_dwc=None,
            specimen_xml=None,
            specimen_tsv=None,
            sequences_fasta=None,
            sequences_trace=None,
            combined_xml=None,
            combined_tsv=None,
            identifiers=identifiers,
            taxonomy=taxonomy,
            specimen_details=specimen_details,
            collection_details=collection_details,
            publications=("", "http"),
            sequence_coi5p=sequence_coi5p,
            electropherogram_traces_coi5p=electropherogram_traces_coi5p,
            attribution=attribution,
        )
        parsed_results.append(result)
    return parsed_results
