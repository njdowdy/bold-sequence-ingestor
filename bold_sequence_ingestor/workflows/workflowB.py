
from functions.bold_async import pull_bold_data_async


async def grab_data_async(write: bool | None = False) -> tuple:
    content = await pull_bold_data_async("specimen", ["Idalus"], to_file=True)
    return content
