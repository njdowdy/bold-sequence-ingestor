import time

from functions.bold_async import pull_bold_data_async, pull_bold_images_async
from utilities.files import write_bold_data
from utilities.json import decode_json


async def grab_data(write: bool | None = False) -> tuple:
    fileFormat, call, content = await pull_bold_data_async("specimen", ["Idalus"])
    if write:
        await write_bold_data(fileFormat.value, call, content)
    return fileFormat, call, content


async def pull_images(content) -> None:
    json_data = await decode_json(content)
    start = time.perf_counter()
    await pull_bold_images_async(json_data, max_results=100)
    print(f"TOTAL TIME: {time.perf_counter() - start}")


async def pull_bold_async(taxa: list[str], **kwargs) -> None:
    start = time.perf_counter()
    await pull_bold_data_async(taxa, kwargs)
    print(f"TOTAL TIME: {time.perf_counter() - start}")
