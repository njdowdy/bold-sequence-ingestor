from enum import Enum

from models.base import BOLDSingleSearch


class BOLDv3SearchEnum(str, Enum):
    sequence: str = "sequence"
    specimen: str = "specimen"
    combined: str = "combined"
    trace: str = "trace"


class BOLDv3FileFormatEnum(str, Enum):
    tsv: str = "tsv"
    xml: str = "xml"


class BOLDv3SingleSearch(BOLDSingleSearch):
    version: str = "v3"
    search_type: BOLDv3SearchEnum
    fileFormat: BOLDv3FileFormatEnum | None = BOLDv3FileFormatEnum.tsv
