from re import S
from pydantic import BaseModel, HttpUrl


class BOLDv4RecordViewIdentifiers(BaseModel):
    sampleid: str
    fieldnum: str | None
    institution_storing: str
    museumid: str | None
    collection_code: str | None


class BOLDv4RecordViewTaxonomy(BaseModel):
    phylum: str
    classs: str
    order: str
    family: str
    subfamily: str
    genus: str
    species: str
    subspecies: str | None
    bin_uri: str


class BOLDv4RecordViewSpecimenDesc(BaseModel):
    voucher_status: str | None
    reproduction: str | None
    sex: str | None
    lifestage: str | None
    tissue_descriptor: str | None
    brief_note: str | None
    detailed_notes: str | None


class BOLDv4RecordViewCollectingEvent(BaseModel):
    collectors: str | None
    date_collected: str | None
    country: str
    province_state: str | None
    region_county: str | None
    sector: str | None
    exactsite: str | None
    lat: str | None
    lon: str | None
    coord_source: str | None
    coord_accuracy: str | None
    elev: str | None
    elev_accuracy: str | None
    depth: str | None
    depth_accuracy: str | None


class BOLDv4RecordViewSequenceInfo(BaseModel):
    funding_source: str | None
    sequence_id: str
    last_updated: str
    genbank_accession: tuple[str, HttpUrl]
    genome: str
    locus: str
    nucleotides: int
    dna_sequence: str
    aa_sequence: str


class BOLDv4RecordViewElectropherogram(BaseModel):
    length: int
    pcr_primers: str
    read_direction: str
    status: str
    run_date: str
    link: HttpUrl


class BOLDv4RecordViewAttribution(BaseModel):
    specimen_depository: str
    sequencing_center: str
    photography: str
    collectors: str
    specimen_identification: str
    project_manager: str
    sequencing_support: str


class BOLDv4RecordView(BaseModel):
    specimen_dwc: HttpUrl | None
    specimen_xml: HttpUrl | None
    specimen_tsv: HttpUrl | None
    sequences_fasta: HttpUrl | None
    sequences_trace: HttpUrl | None
    combined_xml: HttpUrl | None
    combined_tsv: HttpUrl | None
    identifiers: BOLDv4RecordViewIdentifiers
    taxonomy: BOLDv4RecordViewTaxonomy
    specimen_details: BOLDv4RecordViewSpecimenDesc
    collection_details: BOLDv4RecordViewCollectingEvent
    publications: list[tuple[str, HttpUrl]]
    sequence_coi5p: BOLDv4RecordViewSequenceInfo
    electropherogram_traces_coi5p: list[BOLDv4RecordViewElectropherogram]
    attribution: BOLDv4RecordViewAttribution
