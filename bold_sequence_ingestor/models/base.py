from enum import Enum

from pydantic import BaseModel, HttpUrl


class BOLDVersion(str, Enum):
    v3: str = "v3"
    v4: str = "v4"


class BOLDSingleSearch(BaseModel):
    taxon: str | None = None
    ids: str | None = None
    bins: str | None = None
    container: str | None = None
    institutions: str | None = None
    researchers: str | None = None
    geo: str | None = None
    marker: str | None = None


class BOLDApiCall(BaseModel):
    url: HttpUrl
