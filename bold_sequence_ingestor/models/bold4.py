from enum import Enum

from pydantic import BaseModel, HttpUrl

from models.base import BOLDSingleSearch


class BOLDv4dataTypeEnum(str, Enum):
    overview: str = "overview"
    drill_down: str = "drill_down"


class BOLDv4SearchEnum(str, Enum):
    sequence: str = "sequence"
    specimen: str = "specimen"
    combined: str = "combined"
    trace: str = "trace"
    stats: str = "stats"


class BOLDv4SpecimenDownloadEnum(str, Enum):
    tsv: str = "tsv"
    xml: str = "xml"
    json: str = "json"
    dwc: str = "dwc"


class BOLDv4FileFormatEnum(str, Enum):
    json: str = "json"
    xml: str = "xml"


class BOLDv4SingleSearch(BOLDSingleSearch):
    version: str = "v4"
    search_type: BOLDv4SearchEnum
    dataType: BOLDv4dataTypeEnum | None
    fileFormat: BOLDv4FileFormatEnum | None = BOLDv4FileFormatEnum.json
    specimen_download: BOLDv4SpecimenDownloadEnum | None = (
        BOLDv4SpecimenDownloadEnum.tsv
    )


class BOLDv4Coordinate(BaseModel):
    lat: str
    lon: str
    coord_source: str | None
    coord_accuracy: str | None


class BOLDv4Taxon(BaseModel):
    taxID: str
    name: str
    reference: str | None


class BOLDv4CollectingEvent(BaseModel):
    collectors: str
    country: str
    province_state: str | None
    region: str | None
    sector: str | None
    exactsite: str | None
    coordinates: BOLDv4Coordinate | None
    elev: str | None


class BOLDv4SpecimenDesc(BaseModel):
    voucher_status: str | None
    reproduction: str | None
    sex: str | None
    lifestage: str | None
    extra_info: str | None


class BOLDv4SpecimenIdentifiers(BaseModel):
    sampleid: str
    catalognum: str | None
    fieldnum: str | None
    institution_storing: str


class BOLDv4Copyright(BaseModel):
    copyright_holder: str | None
    copyright_year: str | None
    copyright_license: str | None
    copyright_institution: str | None


class BOLDv4MediaItem(BaseModel):
    mediaID: int
    media_descriptor: str | None
    copyright: BOLDv4Copyright | None
    photographer: str | None
    image_file: HttpUrl


class BOLDv4SpecimenImages(BaseModel):
    media: list[BOLDv4MediaItem]


class BOLDv4Taxonomy(BaseModel):
    identification_provided_by: str | None
    phylum: dict[str, BOLDv4Taxon]
    classs: dict[str, BOLDv4Taxon] | None
    order: dict[str, BOLDv4Taxon] | None
    family: dict[str, BOLDv4Taxon] | None
    subfamily: dict[str, BOLDv4Taxon] | None
    genus: dict[str, BOLDv4Taxon] | None
    species: dict[str, BOLDv4Taxon] | None


class BOLDv4Result(BaseModel):
    record_id: str
    processid: str
    bin_uri: str
    specimen_identifiers: BOLDv4SpecimenIdentifiers | None
    taxonomy: BOLDv4Taxonomy
    specimen_desc: BOLDv4SpecimenDesc | None
    collection_event: BOLDv4CollectingEvent
    specimen_imagery: BOLDv4SpecimenImages | None
    notes: str | None
